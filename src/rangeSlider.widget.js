import * as dom from "./dom.js";
import {buildNode} from "./dom.js";
import * as n from "./number.js";
import {formatNumber, parseNumber} from "./number.js";

export default RangeSlider;

function RangeSlider(options){
  if(typeof options === 'undefined') options = {};
  // dummy for ide :
  options.pubKey = options.pubKey || undefined;
  options.pubkey = options.pubkey || undefined;
  options.receiverAccount = options.receiverAccount || undefined;
  options.about = options.about || undefined;
  options.fixPrice = options.fixPrice || undefined;
  options.suggestedValue = options.suggestedValue || undefined;
  options.initialMarker = options.initialMarker || undefined;
  options.minMarker = options.minMarker || undefined;
  options.maxMarker = options.maxMarker || undefined;
  options.forceToMarker = options.forceToMarker || undefined;
  // end dummy

  options.initialValue = options.value || options.defaultValue || options.initialValue || options.suggestedValue ;
  options.min = options.minValue || options.min;
  options.max = options.maxValue || options.max;
  options.wallet = options.wallet || options.pubKey || options.pubkey || options.uid || options.receiverAccount;
  options.description = options.about || options.description;
  options.readonly = options.readonly || options.fixPrice;
  options.required = options.required || options.locked || options.readonly;
  options.percent = options.percent || options.ratio;
  options.unit = options.unit || options.percent?'%':'Ǧ1';
  options.markers = options.markers || {};

  options.__noMarkers__ = !Object.keys(options.markers).length;
  options.__initialValue__ = parseNumber(options.initialValue);
  options.__min__ = options.min?parseNumber(options.min):0;
  options.__max__ = options.max?parseNumber(options.max):(options.percent?100:false);
  if(options.forceToMarker){
    const markersValues = Object.keys(options.markers);
    options.__min__ = parseNumber(markersValues[0]) || options.__min__;
    options.__max__ = parseNumber(markersValues[markersValues.length-1]) || options.__max__;
    if(!options.__initialValue__) options.__initialValue__ = options.__min__;
  }

  const widget = widgetRoot(options);
  widget.addUpdateAction = genAddUpdateAction(widget);
  widget.updateByFunc = genUpdateByFunc(widget);




  widget.appendChild(titleArea(options));
  widget.appendChild(actionButton(options,widget));
  widget.appendChild(inputArea(options,widget));
  if(options.readonly) widget.set = ()=>false;
  else{
    widget.set = genSetValue(widget,options);
    widget.appendChild(sliderArea(options, widget));
    //wheelInteraction(widget,n,widget.updateByFunc);
  }
  widget.set(options.__initialValue__);
  return widget;
}
function sliderInteraction(sliderRoot,options,widget){

  const rangeClickPosition = (e)=>{
    const rangeBaseWidth = sliderRoot.offsetWidth;
    const rangeBaseLeft = dom.left(sliderRoot);
    const converter = n.genScalePxPosition2Value(rangeBaseLeft,rangeBaseLeft+rangeBaseWidth,options.__min__,sliderMax(options));
    return converter(e.clientX);
  };
  const mouseRangeListenerAction = (e)=>widget.set(rangeClickPosition(e));
  sliderRoot.addEventListener('click',mouseRangeListenerAction);

  const cancelMouseListening = ()=>{
    //window.removeEventListener('touchmove',mouseRangeListenerAction);
    window.removeEventListener('mousemove',mouseRangeListenerAction);
    window.removeEventListener('mouseup',cancelMouseListening);
    //window.removeEventListener('touchend',cancelMouseListening);
  };
  const startDrag = (e)=>{
    widget.set(rangeClickPosition(e));
    window.addEventListener('mousemove',mouseRangeListenerAction);
    //window.addEventListener('touchmove',mouseRangeListenerAction);
    //window.addEventListener('touchend',cancelMouseListening);
    window.addEventListener('mouseup',cancelMouseListening);
  };
  //sliderRoot.addEventListener('touchstart',startDrag);
  sliderRoot.addEventListener('mousedown',startDrag);

}

function widgetRoot(options){
  const widget = buildNode('.rangeSlider');
  if(options.readonly){
    widget.classList.add('fixPrice');
    widget.classList.add('readonly');
    widget.classList.add('locked');
  }
  if(options.required) widget.classList.add('required');
  if(options.percent) widget.classList.add('percent');
  if(options.__min__) widget.classList.add('never0');
  return widget;
}
function genUpdateByFunc(obj){
  return (updateFunc)=>obj.set(updateFunc(obj.get()));
}
function titleArea(options){
  const label = buildNode('label');
  label.appendChild(buildNode('span.title',options.title));
  if(options.description){
    const about = buildNode('.about','?');
    const desc = buildNode('.description',options.description);
    about.appendChild(desc);
    label.appendChild(about);
    about.addEventListener('click',()=>about.classList.toggle('show'));
    about.addEventListener('mouseover',()=>{
      const widget = about.parentNode.parentNode;
      const leftAbout = dom.left(about,widget);
      const widthAbout = about.offsetWidth;
      const widgetWidth = widget.offsetWidth;
      const availableWidth = widgetWidth-leftAbout-widthAbout;
      desc.style.width = availableWidth-widget.offsetHeight-5+'px';
    });
  }
  const walletNode = buildNode('a.wallet.key',`🔑 ${firstNchar(options.wallet,8)}`);
  walletNode.setAttribute('href',`g1://wallet:${options.wallet}`);
  label.appendChild(walletNode);
  return label;
}
function actionButton(options,effectRootNode){
  if(options.required) return lockedButton();
  else return removeButton(effectRootNode);
}
function lockedButton() {
  const action = buildNode('.action.locked');
  action.appendChild(buildNode('.picto', '🔒'));
  return action;
}
function removeButton(removableRoot) {
  const action = buildNode('.action.remove');
  const removeWaiter = document.createElementNS("http://www.w3.org/2000/svg", 'svg');
  removeWaiter.setAttribute("viewBox", "0 0 10 10");
  removeWaiter.innerHTML = `<circle cx="5" cy="5" r="4"/>`;
  action.appendChild(removeWaiter);
  const picto = buildNode('.picto', '×');
  action.appendChild(picto);
  action.appendChild(buildNode('.recover', 'Restaurer'));
  removeButtonInteraction(action,removableRoot);
  return action;
}
function removeButtonInteraction(button,removableRoot){
  button.addEventListener('click', () => {
    const classes = removableRoot.classList;

    if (!classes.contains('removed') && !classes.contains('recoverable')) {
      classes.add('removed');
      button.querySelector('.picto').innerText = '+';
      button.timer = setTimeout(() =>{
        classes.add('recoverable');
        classes.remove('removed');
        button.timer = setTimeout(() =>{
          removableRoot.remove();
        }, 30000);//30s
      }, 400);
    } else {
      clearTimeout(button.timer);
      classes.remove('recoverable');
      classes.remove('removed');
      button.querySelector('.picto').innerText = '×';
    }
  });

}

function sliderMarkers(options){
  const v2p = genSliderValue2PercentPosition(options);

  const markers = buildNode('.markers');

  if(options.__noMarkers__){
    options.markers[options.__min__] = options.minMarker || options.__min__+'<span class="unit"> '+options.unit+'</span>';
    if(options.initialMarker) options.markers[options.__initialValue__] = options.initialMarker || options.__initialValue__;
    if(options.__max__ || options.maxMarker)
      options.markers[options.__max__] = options.maxMarker || options.__max__+'<span class="unit"> '+options.unit+'</span>';
  }

  Object.keys(options.markers).forEach((value,index)=>{
    const label = options.markers[value];

    if(index===0){
      const marker = buildNode('.left.border', label);
      marker.style.left = v2p(parseNumber(value));
      markers.appendChild(marker);
    } else if (index===Object.keys(options.markers).length-1 && (parseNumber(value)===options.__max__ || options.__noMarkers__)){
      if(options.__noMarkers__ && !options.__max__){
        const marker = buildNode('.right', label);
        marker.style.right = '0';
        markers.appendChild(marker);
      } else{
        const marker = buildNode('.right.border', label);
        marker.style.right = (100-parseFloat(v2p(parseNumber(value))))+'%';
        markers.appendChild(marker);
      }
    } else {
      const marker = buildNode('.left.border', label);
      marker.style.left = v2p(parseNumber(value));
      markers.appendChild(marker);
    }

  });

  return markers;
}
function sliderMax(options){
  const markersMax = options.__noMarkers__?false:Object.keys(options.markers).reduce((best, curr)=>Math.max(best,parseNumber(curr)),-Infinity);
  return options.__max__ || markersMax || n.roundX(options.__initialValue__*10,1) || 100;
}
function genSliderValue2PercentPosition(options){
  const v2p = n.genScaleValue2PercentPosition(options.__min__,sliderMax(options));
  return (v) => v2p(v)+'%';
}
function sliderRangeProgress(options,widget) {
  const rangeProgress = buildNode('.rangeProgress');
  const v2p = genSliderValue2PercentPosition(options);
  widget.addUpdateAction((v)=>{
    rangeProgress.style.width = v2p(v);

    rangeProgress.classList.remove('poorest','poor','medium','generous','rich','max');
    if(v<=(options.__initialValue__/10)) rangeProgress.classList.add('poorest');
    else if(v<(options.__initialValue__/2)) rangeProgress.classList.add('poor');
    else if(v>=sliderMax(options)) rangeProgress.classList.add('max');
    else if(v>=(options.__initialValue__*5)) rangeProgress.classList.add('rich');
    else if(v>=options.__initialValue__) rangeProgress.classList.add('generous');
    else rangeProgress.classList.add('medium');
  });

  return rangeProgress;
}
function sliderArea(options, widget){
  const slider = buildNode('.slider');

  slider.appendChild(sliderMarkers(options));
  slider.appendChild(buildNode('.rangeBase')); //visual
  slider.appendChild(sliderRangeProgress(options, widget));
  slider.appendChild(sliderCursor(options, widget));

  sliderInteraction(slider,options,widget);
  return slider;
}
function sliderCursor(options,widget){
  const rangeCursorArea = buildNode('.rangeCursorArea');
  const rangeCursor = buildNode('.rangeCursor',"◀&nbsp;|&nbsp;▶");
  rangeCursorArea.appendChild(rangeCursor);
  const v2p = genSliderValue2PercentPosition(options);
  widget.addUpdateAction((v)=>{
    rangeCursorArea.style.left = v2p(v);
  });

  return rangeCursorArea;
}


function inputArea(options,widget){
  const inputArea = buildNode('.inputArea');
  inputArea.appendChild(inputField(options,widget));
  inputArea.appendChild(inputUnit(options));
  if(!options.readonly) {
    inputArea.appendChild(inputButtonMore(widget));
    inputArea.appendChild(inputButtonLess(widget));
  }
  return inputArea;
}
function inputField(options,widget){
  const input = buildNode('input');
  input.value = formatNumber(options.__initialValue__);
  if(options.required){
    input.setAttribute('required',true);
    input.classList.add('required');
  }
  if(options.readonly){
    input.setAttribute('readonly',true);
    input.classList.add('readonly');
    widget.get = ()=>options.__initialValue__;
  } else widget.get = genGetValue(input);

  widget.addUpdateAction((v)=>input.value = formatNumber(v));
  inputKeyInteraction(input,n,genUpdateByFunc(widget));
  return input;
}
function inputKeyInteraction(monitored,directionLib,updater){
  //TODO: forceToMarker mode
  monitored.addEventListener('keydown',(e)=>{
    if(e.code==="ArrowUp" || e.code==="ArrowDown") {
      const direction = (e.code === "ArrowUp")?directionLib.bigger:directionLib.smaller;
      if(e.shiftKey) return updater(direction.significative1);
      if(e.altKey) return updater(direction.cent);
      return updater(direction.unit);
    }
  });
  const identity = (n)=>n;
  const apply = ()=>updater(identity);
  monitored.addEventListener('change',apply);
}
function inputUnit(options){
  const unit = buildNode('.unit',options.unit);
  unit.addEventListener('click',()=>unit.parentNode.querySelector('input').focus());
  return unit;
}
function inputButtonMore(widget){
  return inputButton(widget,true);
}
function inputButtonLess(widget){
  return inputButton(widget,false);
}
function inputButton(widget,addMore=true){
  const button = buildNode(`.${addMore?'more':'less'}.button`,addMore?'+':'-');
  inputButtonInteraction(button,addMore?n.bigger:n.smaller, genUpdateByFunc(widget));
  return button;
}
function inputButtonInteraction(button,direction, updater){
  const startClick = ()=>{
    button.startTimer = Date.now();
    button.timer = setInterval(()=>{
      const elapsed = Date.now() - button.startTimer;
      if(elapsed<500) return;
      if(elapsed<2000) return updater(direction.significative3);
      return updater(direction.significative2);
    },100);
  };
  button.addEventListener('touchstart',startClick);
  button.addEventListener('mousedown',startClick);
  const endClick = ()=>{
    if(!button.startTimer) return;
    clearInterval(button.timer);
    const elapsed = Date.now() - button.startTimer;
    button.startTimer = 0;
    if(elapsed<500){
      return updater(direction.significative1);
    }
  };
  button.addEventListener('touchend',endClick);
  button.addEventListener('mouseup',endClick);
  button.addEventListener('mouseleave',endClick);
}
function genGetValue(valueSource){
  return ()=>parseNumber(valueSource.value);
}
function monetaryBounder(value,min, max=false){
  if(value<min+0.01) return min;
  if(max && value>max-0.01) return max;
  return value;
}
function genSetValue(widget,options){
  if(options.forceToMarker)
    return (newValue)=>{
      const forcedValue = parseNumber(Object.keys(options.markers).map((k)=>[Math.abs(newValue - parseNumber(k)),k]).reduce((best,current)=>current[0]<best[0]?current:best,[+Infinity,0])[1]);
      widget.updateActions.forEach( u=>u(forcedValue) );
    };
  else return (newValue)=>{
    const boundedValue = monetaryBounder(newValue,options.__min__, options.__max__);
    widget.updateActions.forEach( u=>u(boundedValue) );
  }
}
function genAddUpdateAction(widget){
  if(!widget.updateActions) widget.updateActions = [];
  return (updateAction)=>widget.updateActions.push(updateAction);
}
function firstNchar(str,max){
  if(!str) return str;
  return (str+'').substr(0,max);
}
function wheelInteraction(monitored,directionLib,updater){
  monitored.addEventListener('wheel',(e)=>{
    e.preventDefault();
    const direction = (e.deltaY<0)?directionLib.bigger:directionLib.smaller;
    return updater(direction.significative1);
  });
}
