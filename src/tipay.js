import * as dom from "./dom.js";
import {buildNode} from "./dom.js";
import rangeSliderWidget from "./rangeSlider.widget.js";


document.querySelectorAll('template.tipay').forEach((n) => {
  const conf = JSON.parse(n.innerHTML);
  const fieldset = buildNode('fieldset');
  fieldset.appendChild(buildNode('legend', `Tipǎy ${conf.title ? ' : ' + conf.title : ''}`));
  conf.lines.forEach((l) => fieldset.appendChild(rangeSliderWidget(l)));
  dom.outpend(n, fieldset);
});

// Tipay pas ma tete
document.body.appendChild(rangeSliderWidget({
  title: "La nuit chez bibi",
  description: `Tout compris, c'est pas ici.`,
  wallet: "dlcHome",
  forceToMarker:true,
  value:600,
  markers:{
    100:"chambre ouverte",
    200:"avec lit",
    300:"avec draps",
    400:"avec lumière",
    500:"avec WC",
    600:"avec douche",
    700:"avec petit-dej",
    800:"avec le sourire",
    1100:"avec champagne"
  }
}));
document.body.appendChild(rangeSliderWidget({
  max: '10',
  value: '1',
  title: "Prime de licenciement pour le personnel",
  description: `Grace au gouvernement je peux choisir le montant !`,
  wallet: "au black",
  markers:{
    1:'1',
    2:'2',
    3:'3',
    4:'4',
    5:'5',
    6:'6',
    7:'7',
    8:'8',
    9:'9',
  }
}));
