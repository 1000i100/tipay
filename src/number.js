export {parseNumber,formatNumber,anyX,ceilX,floorX,roundX,smaller,bigger,genScaleValue2PercentPosition,genScalePxPosition2Value}

function parseNumber(str){
    if(!str) return 0;
    str+='';
    const float = parseFloat(str.replace(',','.').replace(/ /g,''));
    const coef=' KMGTPEZY';
    for(let mult=1;mult<=coef.length;mult++)
        if(str.toUpperCase().indexOf(coef.substr(mult,1))>0)
            return parseFloat( float+'e+'+(mult*3) );
    return float;
}
function formatNumber(num){
    if(num>=1e+29) return num;
    const coef=' kMGTPEZY';
    for(let precision = 1;precision<=9;precision++){
        if(floorX(num,precision)===floorX(num,10)){
            for(let pow=24;pow>=3;pow-=3){
                if(Math.floor(num/Math.pow(10,pow))>=Math.pow(10,precision-1))
                    return kspace(r(num,pow))+' '+coef.substr(Math.floor(pow/3),1);
            }
        }
    }
    if(Math.floor(num/1000)>0) return kspace(num);
    return formatCent(num);

    function r(num, coef){
        return Math.round(num/Math.pow(10,coef));
    }
    function kspace(num){
        const k = Math.floor(num/1000);
        if(k>0) return kspace(k)+' '+unit3(Math.round(num)-k*1000);
        return num;
    }
    function unit3(num){
        if(num>=100) return ''+num;
        if(num>=10) return '0'+num;
        return '00'+num;
    }
    function formatCent(num){
      if(Math.round(num) === Math.round(num*100)/100) return ''+Math.round(num*100)/100;
      let res = (Math.round(num*100)/100+'').split('.');
      if(res[1].length===1) res[1]+='0';
      return res.join(',');
    }
}

function roundX(number, precision=1){
    return anyX(number,precision,Math.round);
}
function floorX(number, precision=1){
    return anyX(number,precision,Math.floor);
}
function ceilX(number, precision=1){
    return anyX(number,precision,Math.ceil);
}
function anyX(number, precision,anyFunc){
    //if(precision>4) throw 'fail with 1e+21 number form';
    if(number<0) return 0;
    let rounded = anyFunc(number*100).toExponential(10)*1;
    let e=0;
    while((rounded+'').length>precision || (rounded+'').indexOf('e+')>0){
        e++;
        rounded = anyFunc(rounded/10).toExponential(10)*1;
    }
    if(e>2)  return parseFloat(rounded+'e+'+(e-2));
    return parseFloat(rounded+'e+'+e)/100;
}
function genScaleValue2PercentPosition(vMin=0, vMax, func=Math.sqrt){
  return (value) => {
    if(value<vMin) return 0;
    if(value>vMax) return 100;
    return func((value-vMin)/(vMax-vMin))*100;
  }
}
function pow2(v){return Math.pow(v,2);}
function genScalePxPosition2Value(pxMin,pxMax,vMin,vMax,func=pow2){
  const pxPos = (px) => (px-pxMin) / (pxMax-pxMin);
  const scaleChange = (ratio) => vMin + (vMax-vMin) * func( ratio );
  return (px)=>{
    if(px<pxMin) return vMin;
    if(px>pxMax) return vMax;
    return scaleChange(pxPos(px));
  }
}
function smallerSignificative1(n){
  if(floorX(n,1)<n) return floorX(n,1);
  if(floorX(n-0.01,1)<n) return floorX(n-0.01,1);
  return floorX(n*0.99999,1);
}
function biggerSignificative1(n) {
  if(ceilX(n,1)>n) return ceilX(n,1);
  if(ceilX(n+0.01,1)>n) return ceilX(n+0.01,1);
  return ceilX(n*1.00001,1);
}
const bigger = {
  "cent":(n)=>n+0.01,
  "unit":(n)=>n+1,
  "significative1":biggerSignificative1,
  "significative2":(n)=>Math.max(1,ceilX(n*1.1,2)),
  "significative3":(n)=>Math.max(0.1,ceilX(n*1.01,3))
};
const smaller = {
  "cent":(n)=>n-0.01,
  "unit":(n)=>n-1,
  "significative1":smallerSignificative1,
  "significative2":(n)=>floorX(n*0.9,2),
  "significative3":(n)=>floorX(n*0.99,3)
};
