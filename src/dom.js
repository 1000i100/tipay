export {left,outpend,buildNode,addOrReplace,addOnce}
export default buildNode;

function outpend(refElem,newParentNode){
  const parent = refElem.parentNode;
  parent.replaceChild(newParentNode, refElem);
  newParentNode.appendChild(refElem);
}

function left(node,ancestorToStop=false){
  if(!node.offsetParent || node.offsetParent === node || node.offsetParent === ancestorToStop) return 0;
  return node.offsetLeft+left(node.offsetParent,ancestorToStop);
}

function addOrReplace(me,here){
  const used = here.querySelector('#'+me.id);
  if(used)here.removeChild(used);
  here.appendChild(me);
}

function addOnce(me,here){
  if(!here.querySelector('#'+me.id)) here.appendChild(me);
}

function buildNode(tag, content) {
  let realTag = "div";
  if(tag){
    const tagFound = tag.match(/^[^.#]+/);
    if(tagFound) realTag = tagFound[0];
  }
  const node = document.createElement(realTag);
  if(tag) {

    // add id
    const idFound = tag.match(/#[^.#]+/);
    if (idFound) node.id = idFound[0].substring(1);

    //add classes
    const classes = tag.match(/\.[^.#]+/g);
    if (classes) classes.forEach((c) => node.classList.add(c.substring(1)));
  }

  //content builder
  if (realTag === 'input') node.value = content;
  else if (typeof content !== "undefined") node.innerHTML = content;

  return node;
}
