import * as app from "./number.js";

describe('parseNumber',()=>{
  it('decimal number with "." and "," ', () => {
    expect( app.parseNumber('0.01') ).toBe(0.01);
    expect( app.parseNumber('9,99') ).toBe(9.99);
  });
  it('number with thousan space separator', () => {
    expect( app.parseNumber('1 000') ).toBe(1000);
    expect( app.parseNumber('35 938,20') ).toBe(35938.2);
  });
  it('number with coefficient', () => {
    expect( app.parseNumber('1 k') ).toBe(1000);
    expect( app.parseNumber('1 K') ).toBe(1000);
    expect( app.parseNumber('35 T') ).toBe(35000000000000);
  });
  it('very big float', () => {
    expect( app.parseNumber('82e+30') ).toBe(8.2e+31);
  });
});
describe('formatNumber',()=>{
  it('display decimal number with "," ', () => {
    expect( app.formatNumber(0.01) ).toBe('0,01');
  });
  it('display 2 decimal number or none', () => {
    expect( app.formatNumber(0.1) ).toBe('0,10');
    expect( app.formatNumber(9.001) ).toBe('9');
  });
  it('shortcut number with coefficient when possible', () => {
    expect( app.formatNumber(1234567890) ).toBe('1 234 567 890');
    expect( app.formatNumber(1234567000) ).toBe('1 234 567 k');
    expect( app.formatNumber(1234000000) ).toBe('1 234 M');
    expect( app.formatNumber(1200000000) ).toBe('1 200 M');
    expect( app.formatNumber(1000000000) ).toBe('1 G');
  });
  it('big number fix',()=>{
    const input = '2 699 999 999 999 999 512 000 000 000';
    const output = '2 700 Y';
    expect( app.formatNumber(app.ceilX(app.parseNumber(input),3)) ).toBe(output);
  });

});
describe('anyX, floorX, ceilX, roundX',()=>{
  it('keep only 1 significative number by default', () => {
    expect( app.floorX(1234567890) ).toBe(1000000000);
    expect( app.ceilX(1234567890) ).toBe(2000000000);
    expect( app.roundX(1234567890) ).toBe(1000000000);
    expect( app.roundX(9876543210) ).toBe(10000000000);
  });
  it('keep as many significative number as asked', () => {
    expect( app.floorX(1234567890,6) ).toBe(1234560000);
    expect( app.ceilX(1234567890,3) ).toBe(1240000000);
    expect( app.roundX(1234567890,2) ).toBe(1200000000);
    expect( app.roundX(1234567890,5) ).toBe(1234600000);
  });
});
describe('rangeSlider conversions',()=>{
  it('value2position', () => {
    const v2p = app.genScaleValue2PercentPosition(0,100);
    expect( v2p(0) ).toBe(0);
    expect( v2p(100) ).toBe(100);
    expect( v2p(9) ).toBe(30);
  });
  it('value2position with min not null', () => {
    const v2p = app.genScaleValue2PercentPosition(10,100);
    expect( v2p(10) ).toBe(0);
    expect( v2p(100) ).toBe(100);
  });
  it('value2position with magic number', () => {
    const v2p = app.genScaleValue2PercentPosition(0.99,4.99);
    expect( v2p(0.99) ).toBe(0);
    expect( v2p(4.99) ).toBe(100);
  });
  xit('position2value', () => {
    //TODO test position2value.
    expect( app.roundX(1234567890,5) ).toBe(1234600000);
  });
});
